// Dark mode toggle
const darkModeToggle = document.getElementById("dark-mode-toggle");
const body = document.body;

// Set dark mode preference from localStorage
if (localStorage.getItem("darkMode") === "enabled") {
    enableDarkMode();
} else {
    disableDarkMode();
}

darkModeToggle.addEventListener("click", () => {
    if (body.classList.contains("dark-mode")) {
        disableDarkMode();
        localStorage.setItem("darkMode", "disabled");
    } else {
        enableDarkMode();
        localStorage.setItem("darkMode", "enabled");
    }
});

function enableDarkMode() {
    body.classList.add("dark-mode");
    darkModeToggle.innerHTML = '<i class="fas fa-sun"></i>';
}

function disableDarkMode() {
    body.classList.remove("dark-mode");
    darkModeToggle.innerHTML = '<i class="fas fa-moon"></i>';
}

// Back to top button
const backToTopButton = document.querySelector(".back-to-top");

backToTopButton.addEventListener('click', () => {
    window.scrollTo({
        top: 0,
        left: 0,
        behavior: 'smooth'
    });
});

window.addEventListener('scroll', () => {
    if (window.scrollY > 0) {
        backToTopButton.classList.add('show');
    } else {
        backToTopButton.classList.remove('show');
    }
});


function generateProjectSection(id, title, description, homepage) {
    var section = document.createElement('section');
    section.id = id;
    section.className = 'project';

    var projectTitle = document.createElement('h2');
    projectTitle.className = 'project-title';
    projectTitle.textContent = title;

    var projectDescription = document.createElement('p');
    projectDescription.className = 'project-description';
    projectDescription.textContent = description;

    var projectLink = document.createElement('p');
    projectLink.className = 'project-link';

    var projectLinkAnchor = document.createElement('a');
    projectLinkAnchor.href = homepage;
    projectLinkAnchor.textContent = title + ' Homepage';

    projectLink.appendChild(projectLinkAnchor);

    section.appendChild(projectTitle);
    section.appendChild(projectDescription);
    section.appendChild(projectLink);

    return section;
}


function addProjectToNavigation(id, title) {
    var navElement = document.querySelector('nav>ul');
    var navList = document.createElement('li');
    var navLink = document.createElement('a');
    navLink.href = '#' + id;
    navLink.textContent = title;
    navList.appendChild(navLink);
    navElement.appendChild(navList);
}



$(document).ready(function () {
    // Fetch repository information using GitLab API
    $.ajax({
        url: 'https://gitlab.com/api/v4/projects/rodolfo-verde%2Fpallet-simulator',
        dataType: 'json',
        success: function (data) {
            var projectDescription = data.description;
            var mainElement = document.querySelector('main');
            var project = generateProjectSection('project1', 'Pallet Simulator', projectDescription, data.web_url);
            mainElement.appendChild(project);
            // add the project to the navigation bar
            addProjectToNavigation('project1', 'Pallet Simulator')
        },
        error: function () {
            var mainElement = document.querySelector('main');
            var project = generateProjectSection('project1', "Pallet Simulator", "Failed to load project description.", "https://gitlab.com/rodolfo-verde/pallet-simulator");
            mainElement.appendChild(project);
        }
    });

    // Fetch repository information using GitHub API
    $.ajax({
        url: 'https://api.github.com/repos/Darshanjain957/Vinebot_noetic_packages',
        dataType: 'json',
        success: function (data) {
            var projectDescription = data.description;
            
            var mainElement = document.querySelector('main');
            var project = generateProjectSection('project2', 'Vinebot Noetic Packages', projectDescription, data.html_url);
            mainElement.appendChild(project);

            // add the project to the navigation bar
            addProjectToNavigation('project2', 'Vinebot Noetic Packages')
        },
        error: function () {
            var mainElement = document.querySelector('main');
            var project = generateProjectSection('project2', "Vinebot Noetic Packages", "Failed to load project description.", "https://api.github.com/repos/Darshanjain957/Vinebot_noetic_packages");
            mainElement.appendChild(project);
        }
    });
});

// var mainElement = document.querySelector('main');
// var project3 = generateProjectSection('project3', 'Project 3', 'Loading...', 'https://example.com/project3');
// mainElement.appendChild(project3);

